package com.thelsien.projects.griddlerbase.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.thelsien.projects.griddlerbase.R
import com.thelsien.projects.griddlerbase.griddler.GriddlerGrid

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val taskList = ArrayList<Int>()
        val emptyList = ArrayList<Int>()
        taskList.add(2)
        taskList.add(2)
        taskList.add(2)
        emptyList.add(0)

        val rowTasks = ArrayList<List<Int>>()
        rowTasks.add(taskList)
        rowTasks.add(taskList)
        rowTasks.add(emptyList)
        rowTasks.add(taskList)
        rowTasks.add(taskList)
//        rowTasks.add(taskList)
//        rowTasks.add(taskList)
//        rowTasks.add(taskList)
//        rowTasks.add(taskList)
//        rowTasks.add(taskList)
//        rowTasks.add(taskList)

        val columnTasks = ArrayList<List<Int>>()
        columnTasks.add(taskList)
        columnTasks.add(taskList)
        columnTasks.add(emptyList)
        columnTasks.add(taskList)
        columnTasks.add(taskList)

        val grid = view.findViewById<GriddlerGrid>(R.id.grid)
        grid.setTasks(rowTasks, columnTasks)

//        val taskView = view.findViewById<GriddlerTaskView>(R.id.task)
//        taskView.isColumnTaskView = false
//        val taskList = ArrayList<Int>()
//        taskList.add(88)
//        taskList.add(88)
//        taskList.add(88)
//        taskList.add(88)
//        taskList.add(88)
//        taskView.taskList = taskList
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

}
