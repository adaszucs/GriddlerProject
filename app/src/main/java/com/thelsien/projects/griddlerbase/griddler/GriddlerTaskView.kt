package com.thelsien.projects.griddlerbase.griddler

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat

class GriddlerTaskView : ViewGroup {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)

    val maxNumberViewWidth = 18.px
    private val borderPaint = Paint()
    private val resultView = GriddlerResultView(context)

    var isColumnTaskView = false

    var taskList: List<Int> = ArrayList()
        set(value) {
            removeAllViews()

            field = value
            field.forEach {
                val textView = TextView(context)

                //TODO width shouldn't be hard coded, height can only be max. GriddlerGrid.maxCellSize
                textView.layoutParams = if (isColumnTaskView) LayoutParams(30.px, maxNumberViewWidth) else LayoutParams(maxNumberViewWidth, 30.px)
                textView.setTextColor(ContextCompat.getColor(context, android.R.color.black))
                textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10.toFloat())
                textView.gravity = Gravity.CENTER
                //textView.setPadding(10.px, 10.px, 10.px, 10.px)

                textView.text = it.toString()

                addView(textView)
            }

            resultView.isSolved = taskList.size == 1 && taskList[0] == 0
            resultView.isColumnTaskView = this.isColumnTaskView
            addView(resultView)

//            invalidate()
        }

    init {
        //TODO parent check for GriddlerGrid class. If not, throw error.

        borderPaint.color = Color.BLACK
        borderPaint.style = Paint.Style.STROKE
        borderPaint.strokeWidth = 8f //TODO remove magic number
        borderPaint.isAntiAlias = true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        //TODO only maxCellSize size allowed
        var desiredWidth = if (isColumnTaskView) 30.px else 0
        var desiredHeight = if (isColumnTaskView) 0 else 30.px

        measureChildren(widthMeasureSpec, heightMeasureSpec)

        for (i in 0 until childCount) {
            if (isColumnTaskView)
                desiredHeight += getChildAt(i).measuredHeight
            else
                desiredWidth += getChildAt(i).measuredWidth
        }

        val actualWidth = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> Math.min(desiredWidth, widthSize)
            MeasureSpec.UNSPECIFIED -> desiredWidth
            else -> desiredWidth
        }
        val actualHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> Math.min(desiredHeight, heightSize)
            MeasureSpec.UNSPECIFIED -> desiredHeight
            else -> desiredHeight
        }

        setMeasuredDimension(actualWidth, actualHeight)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var childLeft = 0
        var childTop = 0
        var childRight = if (isColumnTaskView) 30.px else maxNumberViewWidth
        var childBottom = if (isColumnTaskView) maxNumberViewWidth else 30.px

        for (i in 0 until childCount) {
            val textView = getChildAt(i)
            textView.layout(childLeft, childTop, childRight, childBottom)

            if (isColumnTaskView) {
                childTop += maxNumberViewWidth
                childBottom += maxNumberViewWidth
            } else {
                childLeft += maxNumberViewWidth
                childRight += maxNumberViewWidth
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
//
//        //border painting
//        canvas?.drawLine(0f, height.toFloat(), width.toFloat(), height.toFloat(), borderPaint)
//        canvas?.drawLine(width.toFloat(), 0f, width.toFloat(), height.toFloat(), borderPaint)
    }
}