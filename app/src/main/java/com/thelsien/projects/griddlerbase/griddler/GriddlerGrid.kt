package com.thelsien.projects.griddlerbase.griddler

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.Toast
import com.thelsien.projects.griddlerbase.R

class GriddlerGrid : ViewGroup {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)

    val maxCellSize = 30.px
    val taskViewFixedWidth = 46.px

    //for the hardened stroke inside the grid
    private val gridPaint = Paint()
    private val gridSize = 5

    private var columns: Int = 0
    private var rows: Int = 0

    private var prevHitBound = Rect()

    var movingCellState: GriddlerCell.CellState = GriddlerCell.CellState.BLANK

    var viewMatrix = null

    init {
        gridPaint.color = Color.BLACK
        gridPaint.strokeWidth = 8f //TODO remove magic number
        gridPaint.style = Paint.Style.STROKE
        gridPaint.isAntiAlias = true
    }

    fun setTasks(rowTasks: List<List<Int>>, columnTasks: List<List<Int>>) {
        this.columns = columnTasks.size
        this.rows = rowTasks.size

        val size = this.columns * this.rows

        if (size <= 0) {
            Toast.makeText(context, R.string.grid_error_size_too_small, Toast.LENGTH_SHORT).show()
            return
        }

        removeAllViews()

        for (i in 0 until rows) {
            for (j in 0 until columns) {
                val cell = GriddlerCell(context)
                cell.rowId = i
                cell.columnId = j
                addView(cell)
            }
        }

        for (i in 0 until rowTasks.size) {
            val taskView = GriddlerTaskView(context)
            taskView.taskList = rowTasks[i]
            addView(taskView)
        }

        for (i in 0 until columnTasks.size) {
            val taskView = GriddlerTaskView(context)
            taskView.isColumnTaskView = true
            taskView.taskList = columnTasks[i]
            addView(taskView)
        }

        invalidate()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)

        val desiredWidth = columns * maxCellSize + taskViewFixedWidth //TODO should be the largest task view's width (hardcoded value)
        val desiredHeight = rows * maxCellSize + taskViewFixedWidth

        val actualWidth = when (widthMode) {
            MeasureSpec.EXACTLY -> widthSize
            MeasureSpec.AT_MOST -> Math.min(desiredWidth, widthSize)
            MeasureSpec.UNSPECIFIED -> desiredWidth
            else -> desiredWidth
        }
        val actualHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> Math.min(desiredHeight, heightSize)
            MeasureSpec.UNSPECIFIED -> desiredHeight
            else -> desiredHeight
        }

        setMeasuredDimension(actualWidth, actualHeight)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var cellLeft = taskViewFixedWidth //TODO should start from the taskview's end
        var cellTop = taskViewFixedWidth
        var taskRowRight = taskViewFixedWidth //TODO only on largest task view addition, smaller should start from largestTaskViewWidth - taskViewWidth
        var taskRowTop = taskViewFixedWidth
        var taskColumnLeft = taskViewFixedWidth
        var taskColumnBottom = taskViewFixedWidth
        for (i in 0 until childCount) {
            val childView = getChildAt(i)
            if (childView is GriddlerCell) {
                childView.layout(cellLeft, cellTop, cellLeft + maxCellSize, cellTop + maxCellSize)
                cellLeft += maxCellSize
                if (cellLeft >= width) {
                    cellTop += maxCellSize
                    cellLeft = taskViewFixedWidth //TODO look above comment
                }
            } else if (childView is GriddlerTaskView) {
                //TODO need to check for row or column task view type
                if (childView.isColumnTaskView) {
                    val height = childView.taskList.size * childView.maxNumberViewWidth + 10.px //TODO hardcoded value is result view's fixed width
                    childView.layout(taskColumnLeft, taskColumnBottom - height, taskColumnLeft + maxCellSize, taskColumnBottom)
                    taskColumnLeft += maxCellSize
                } else {
                    val width = childView.taskList.size * childView.maxNumberViewWidth + 10.px //TODO hardcoded value is result view's fixed width
                    childView.layout(taskRowRight - width, taskRowTop, taskRowRight, taskRowTop + maxCellSize)
                    taskRowTop += maxCellSize
                }
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)

        if (columns == 0 || rows == 0) {
            return
        }

        val correctedColumns = if (columns % 5 > 0) columns + (columns % 5) else columns
        val correctedRows = if (rows % 5 > 0) (rows + (rows % 5)) else rows

        //TODO different gridSize for rows and columns?
        val numberOfGrids = (correctedColumns * correctedRows) / (gridSize * gridSize)
        val widthOfGrid = (gridSize * maxCellSize).toFloat()
        val heightOfGrid = (gridSize * maxCellSize).toFloat()
        var gridLeft = taskViewFixedWidth.toFloat()
        var gridTop = taskViewFixedWidth.toFloat()

        for (i in 0 until numberOfGrids) {
            canvas?.drawRect(gridLeft, gridTop, gridLeft + widthOfGrid, gridTop + heightOfGrid, gridPaint)
            gridLeft += (gridSize * maxCellSize)
            if (gridLeft >= width) {
                gridLeft = taskViewFixedWidth.toFloat()
                gridTop += (gridSize * maxCellSize)
            }
        }

        //TODO draw rectangle around task views rows and columns
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_MOVE -> {
                (0 until childCount)
                        .map { getChildAt(it) }
                        .forEach {
                            if (it !is GriddlerCell) {
                                return false
                            }

                            val bounds = Rect()
                            it.getHitRect(bounds)
                            if (bounds != prevHitBound && bounds.contains(ev.x.toInt(), ev.y.toInt())) {
                                prevHitBound = bounds
                                Log.d("GriddlerGrid", "found cell - row: " + it.rowId + " column: " + it.columnId)
                                it.onTouchEvent(ev)
                            }
                        }
            }
        }

        return false
    }
}

//extension for Int to calculate value into dp or px.
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()