package com.thelsien.projects.griddlerbase.griddler

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View

class GriddlerResultView : View {
    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(context, attributeSet, defStyleAttr)

    val maxWidth = 10.px
    val maxHeight = 30.px //TODO should be GriddlerGrid.maxCellSize or parent size, which is a GriddlerTaskView

    var isColumnTaskView = false

    /**
     * Paints the background of the view to red or green according to the
     * {@link GriddlerResultView#isSolved} value.
     */
    val backgroundPaint = Paint()

    /**
     * The value shows that the row or column is solved. Green color means solved, red is unsolved.
     */
    var isSolved = false
        set(value) {
            field = value
            backgroundPaint.color = if (field) Color.GREEN else Color.RED
            invalidate()
        }

    init {
        backgroundPaint.color = Color.RED
        backgroundPaint.style = Paint.Style.FILL
        backgroundPaint.isAntiAlias = true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        //TODO note: this should always be a fixed value, only issue is the zooming in and out.
        if (isColumnTaskView)
            setMeasuredDimension(maxHeight, maxWidth)
        else
            setMeasuredDimension(maxWidth, maxHeight)
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)

        val rect = Rect(0, 0, width, height)
        canvas?.drawRect(rect, backgroundPaint)
    }
}