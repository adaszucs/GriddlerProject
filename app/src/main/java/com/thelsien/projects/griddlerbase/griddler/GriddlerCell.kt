package com.thelsien.projects.griddlerbase.griddler

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View

class GriddlerCell : View {
    var rowId: Int = -1
    var columnId: Int = -1
    val borderPaint: Paint = Paint()
    val fillPaint: Paint = Paint()
    val markerPaint: Paint = Paint()
    var cellState: CellState = CellState.BLANK

    constructor(context: Context) : this(context, null)
    constructor(context: Context, attributeSet: AttributeSet?) : this(context, attributeSet, 0)
    constructor(context: Context, attributeSet: AttributeSet?, defStyAttr: Int) : super(context, attributeSet, defStyAttr)

    init {
        borderPaint.color = Color.BLACK
        borderPaint.strokeWidth = 1.5f
        borderPaint.style = Paint.Style.STROKE
        borderPaint.isAntiAlias = true

        fillPaint.color = Color.BLACK
        fillPaint.style = Paint.Style.FILL

        markerPaint.color = Color.BLACK
        markerPaint.style = Paint.Style.STROKE
        markerPaint.strokeWidth = 5f
        markerPaint.isAntiAlias = true
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (parent !is GriddlerGrid) {
            throw IllegalStateException("Parent of a GriddlerCell should be a GriddlerGrid ViewGroup")
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        val paint = when (cellState) {
            CellState.BLANK -> borderPaint
            CellState.FILLED -> fillPaint
            CellState.MARKED -> markerPaint
        }

        if (paint == markerPaint) {
            val x1 = 3.px.toFloat()
            val x2 = (width - 3.px).toFloat()
            val y1 = 3.px.toFloat()
            val y2 = (height - 3.px).toFloat()

            canvas?.drawLine(x1, y1, x2, y2, paint)
            canvas?.drawLine(x2, y1, x1, y2, paint)
        } else {
            canvas?.drawRect(
                    0.toFloat(),
                    0.toFloat(),
                    width.toFloat(),
                    height.toFloat(),
                    paint
            )
        }

        drawBorder(canvas)
    }

    private fun drawBorder(canvas: Canvas?) {
        canvas?.drawRect(
                0.toFloat(),
                0.toFloat(),
                width.toFloat(),
                height.toFloat(),
                borderPaint
        )
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                cellState = when (cellState) {
                    CellState.BLANK -> CellState.FILLED
                    CellState.FILLED -> CellState.MARKED
                    CellState.MARKED -> CellState.BLANK
                }
                (parent as GriddlerGrid).movingCellState = cellState
            }
            MotionEvent.ACTION_MOVE -> {
                cellState = (parent as GriddlerGrid).movingCellState
            }
        }

        invalidate()
        return true
    }

    override fun onHoverEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_HOVER_ENTER -> event.action = MotionEvent.ACTION_DOWN
            MotionEvent.ACTION_HOVER_MOVE -> event.action = MotionEvent.ACTION_MOVE
            MotionEvent.ACTION_HOVER_EXIT -> event.action = MotionEvent.ACTION_UP
        }

        onTouchEvent(event)
        return true
    }

    override fun performClick(): Boolean {
        return super.performClick()
    }

    enum class CellState {
        BLANK, MARKED, FILLED
    }
}